import os
import pathlib
import cv2
import math
import json
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.sequence import pad_sequences
# import our model, different layers and activation function 
from tensorflow.keras.layers import Dense, LSTM, Reshape, BatchNormalization, Input, Conv2D, MaxPool2D, Lambda, Bidirectional, Add, Activation
from tensorflow.keras.models import Model
from tensorflow.keras.activations import relu, sigmoid, softmax
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import CSVLogger, TensorBoard, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.layers import Reshape, Layer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from keras import initializers, regularizers, constraints


### --- CONSTANTS DEFINING ---
#Set mode of preprocessing data
_BOOL_INVERT_IMG = False
print("\n### --- Set Inverting Image To: {}\n".format(_BOOL_INVERT_IMG))

TIME_STEPS = 182
print("### --- Time Step for processing dataset: {}\n".format(TIME_STEPS))

### --- DEVICE CHECK ---
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
    # RIP Training time
    raise SystemError('GPU device not found')
    print('Found GPU at: {}'.format(device_name))

TRAIN_JSON = './../Data/line_data.json'
RAW_FOLDER = './../Data/line_data/' 


### --- DATA PREPARATION ---
with open(TRAIN_JSON, 'r', encoding='utf8') as f:
    train_labels = json.load(f)
line_data_path = [path for path in train_labels.keys()]
# Get the list of unique character in dataset
char_list_1= set()
for label in train_labels.values():
    char_list_1.update(set(label))
char_list_1=sorted(char_list_1)
''.join(char_list_1)

# --- list of all vietnamese standard characters labels  (185)
# chars = ' abcdđefghiklmnoơpqrstuưvxyăâàảãáạằẳẵặầẫấậêèẻẽéẹềểễếệìỉĩíịòỏõóọôồổỗốộờỡớợùủũúụừửữứựỳỷỹýỵABCDĐEFGHIKLMNOƠPQRSTUƯVXYĂÂÀẢÃÁẠẰẲẴẶẦẪẤẬÊÈẺẼÉẸỀỂỄẾỆÌỈĨÍỊÒỎÕÓỌÔỒỔỖỐỘỜỠỚỢÙỦŨÚỤỪỬỮỨỰỲỶỸÝỴ0123456789'
# char_list_2 = set(chars)

# char_list_2=sorted(char_list_2)

# set_1 = set(char_list_1)
# set_2 = set(char_list_2)

# Combine sets and eliminate duplicates to get a fully standard character labels
# combined_set = set_1.union(set_2)
# ''.join(combined_set)
# combined_set = sorted(combined_set) #got 207
print("\n### --- There are {} Vietnamese tokens...\n".format(len(char_list_1)))


# convert the words to array of indexs based on the char_list
def encode_to_labels(txt):
    # encoding each output word into digits of indexes
    dig_lst = []
    for index, char in enumerate(txt):
        try:
            dig_lst.append(char_list_1.index(char))
        except:
            print("No found in char_list :", char)
    return dig_lst


train_image_path = []

for item in pathlib.Path(RAW_FOLDER).glob('**/*'):
    if item.is_file() and item.suffix not in [".json"]:
        train_image_path.append(str(item))

dict_filepath_label={}
raw_data_path = pathlib.Path(os.path.join(RAW_FOLDER))
for item in raw_data_path.glob('**/*.*'):
    file_name=str(os.path.basename(item))
    if file_name != "labels.json":
        file_name = file_name.split(".")[0]  #only use for train_line dataset
        # print(train_labels[file_name])
        # print(file_name)
        # if file_name not in line_data_path:
            # continue
        label = train_labels[file_name]
        dict_filepath_label[str(item)]=label

label_lens= []
for label in dict_filepath_label.values():
    label_lens.append(len(label))
MAX_LABEL_LEN = max(label_lens)

all_image_paths = list(dict_filepath_label.keys())

TEST_SIZE = 0.2
train_image_paths, val_image_paths = train_test_split(all_image_paths, test_size=TEST_SIZE, random_state=42)


# lists for training dataset
training_img = []
training_txt = []
train_input_length = []
train_label_length = []
orig_txt = []
resize_max_width=0

for train_img_path in tqdm(train_image_paths, desc="Processing Training Images"):
    # print(f_name)
    # read input image and convert into gray scale image
    img = cv2.cvtColor(cv2.imread(train_img_path), cv2.COLOR_BGR2GRAY)
    
    height, width = img.shape

    # in this dataset, we don't need to do any resize at all here.
    img = cv2.resize(img,(int(118/height*width),118))
    
    height, width = img.shape
    
    if img.shape[1] > resize_max_width:
        resize_max_width = img.shape[1]

    img = np.pad(img, ((0,0),(0, 3290-width)), 'median')
    
    # Blur it
    img = cv2.GaussianBlur(img, (5,5), 0)

    # Threshold the image using adapative threshold
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,  
                                # Use THRESH_BINARY
                                 blockSize=11, C=4)
    
    # Invert the image to get black characters on white background
    if _BOOL_INVERT_IMG:
        img = 255 - img  # Invert pixel values
    
    # add channel dimension
    img = np.expand_dims(img , axis = 2)
    
    # Normalize each image
    img = img/255.

    label = dict_filepath_label[train_img_path]

    # split data into validation and training dataset as 10% and 90% respectively
    orig_txt.append(label)   
    train_label_length.append(len(label))

    # our time steps for valid input
    train_input_length.append(TIME_STEPS)
    training_img.append(img)

    # convert words to digits based on charlist
    training_txt.append(encode_to_labels(label)) 


#lists for validation dataset
valid_img = []
valid_txt = []
valid_input_length = []
valid_label_length = []
valid_orig_txt = []

for val_img_path in tqdm(val_image_paths, desc="Processing Validation Images"):
    # print(f_name)
    # read input image and convert into gray scale image
    img = cv2.cvtColor(cv2.imread(val_img_path), cv2.COLOR_BGR2GRAY)
    
    # in this dataset, we don't need to do any resize at all here.
    img = cv2.resize(img,(int(118/height*width),118))
    
    if img.shape[1] > resize_max_width:
        resize_max_width = img.shape[1]
        
    img = np.pad(img, ((0,0),(0, 3290-width)), 'median')
    
    # Blur it
    img = cv2.GaussianBlur(img, (5,5), 0)

    # Threshold the image using adapative threshold
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,  
                                # Use THRESH_BINARY
                                 blockSize=11, C=4)
    
    # Invert the image to get black characters on white background
    if _BOOL_INVERT_IMG:
        img = 255 - img  # Invert pixel values
    
    # add channel dimension
    img = np.expand_dims(img , axis = 2)
    
    # Normalize each image
    img = img/255.

    label = dict_filepath_label[val_img_path]

    valid_orig_txt.append(label)   
    valid_label_length.append(len(label))

    # our time steps for valid input
    valid_input_length.append(TIME_STEPS)
    valid_img.append(img)

    # convert words to digits based on charlist
    valid_txt.append(encode_to_labels(label))

MAX_LABEL_LEN = TIME_STEPS 

# pad each output label to maximum text length, remember we did that so that we keep training with rnn consistent?
train_padded_txt = pad_sequences(training_txt, maxlen=MAX_LABEL_LEN, padding='post', value = 0).astype('int64')
valid_padded_txt = pad_sequences(valid_txt, maxlen=MAX_LABEL_LEN, padding='post', value = 0).astype('int64')

### --- MODEL OF CRNN AND Bi-LSTM WITH ATTENTION ---
# Input layer
inputs = Input(shape=(118, 3290, 1), name='input')

# Block 1
x = Conv2D(64, (3,3), padding='same', name='conv1')(inputs)
x = MaxPool2D(pool_size=3, strides=3, name='pool1')(x)
x = Activation('relu', name='relu1')(x)
x_1 = x 

# Block 2
x = Conv2D(128, (3,3), padding='same', name='conv2')(x)
x = MaxPool2D(pool_size=3, strides=3, name='pool2')(x)
x = Activation('relu', name='relu2')(x)
x_2 = x

# Block 3
x = Conv2D(256, (3,3), padding='same', name='conv3')(x)
x = BatchNormalization(name='batch_norm3')(x)
x = Activation('relu', name='relu3')(x)
x_3 = x

# Block4
x = Conv2D(256, (3,3), padding='same', name='conv4')(x)
x = BatchNormalization(name='batch_norm4')(x)
x = Add(name='add4')([x,x_3])
x = Activation('relu', name='relu4')(x)
x_4 = x

# Block5
x = Conv2D(512, (3,3), padding='same', name='conv5')(x)
x = BatchNormalization(name='batch_norm5')(x)
x = Activation('relu', name='relu5')(x)
x_5 = x

# Block6
x = Conv2D(512, (3,3), padding='same', name='conv6')(x)
x = BatchNormalization(name='batch_norm6')(x)
x = Add(name='add6')([x,x_5])
x = Activation('relu', name='relu6')(x)
x_6 = x

# Block7
x = Conv2D(1024, (3,3), padding='same', name='conv7')(x)
x = BatchNormalization(name='batch_norm7')(x)
x = Activation('relu', name='relu7')(x)
x_7 = x

# Block8
x = Conv2D(1024, (3,3), padding='same', name='conv8')(x)
x = BatchNormalization(name='batch_norm8')(x)
x = MaxPool2D(pool_size=(3, 2), name='pool3')(x)
x = Activation('relu', name='relu8')(x)

x = MaxPool2D(pool_size=(3, 1))(x)
 
squeezed = Lambda(lambda x: K.squeeze(x, 1))(x)
 
blstm_1 = Bidirectional(LSTM(512, return_sequences=True, dropout = 0.2))(squeezed)
blstm_2 = Bidirectional(LSTM(512, return_sequences=True, dropout = 0.2))(blstm_1)

outputs = Dense(len(char_list_1)+1, activation = 'softmax')(blstm_2)
# model to be used at test time
act_model = Model(inputs, outputs)


### --- CTC LOSS DEFINE ---

# define the length of input and label for ctc
labels = Input(name='the_labels', shape=[MAX_LABEL_LEN], dtype='float32')
input_length = Input(name='input_length', shape=[1], dtype='int64')
label_length = Input(name='label_length', shape=[1], dtype='int64')

def ctc_lambda_func(args):
    y_pred, labels, input_length, label_length = args
    
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)
 
# out loss function (just take the inputs and put it in our ctc_batch_cost)
loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([outputs, labels, input_length, label_length])

### --- GET THE FULL MODEL WITH CUSTOM LOSS ---
model = Model(inputs=[inputs, labels, input_length, label_length], outputs=loss_out)


### --- LOAD WEIGHTS OF PRETRAINED
model.load_weights(os.path.join('model_ver17_finetune_v14_on_new_data.h5'))

for layer in model.layers:
    # Check if the layer has weights (not all layers have weights, e.g., Activation layers)
    if layer.weights:
        print(f'Layer: {layer.name}')
        weights = layer.get_weights()
        for i, w in enumerate(weights):
            print(f'  Weights {i + 1}:')
            print(w)


callbacks = [
    TensorBoard(
        log_dir='./logs/ver18_continue_finetune_v14_on_new_data',
        histogram_freq=10,
        profile_batch=0,
        write_graph=True,
        write_images=False,
        update_freq="epoch"),
    ModelCheckpoint(
        filepath="model_ver18_continue_finetune_v14_on_new_data.h5",
        monitor='val_loss',
        save_best_only=True,
        save_weights_only=True,
        verbose=1),
    EarlyStopping(
        monitor='val_loss',
        min_delta=1e-8,
        patience=120,
        restore_best_weights=True,
        verbose=1),
    ReduceLROnPlateau(
        monitor='val_loss',
        min_delta=1e-8,
        factor=0.1,
        patience=30,
        verbose=1)
]
callbacks_list = callbacks

opt = Adam(
    learning_rate=0.0001,
    beta_1=0.9,
    beta_2=0.999,
    epsilon=1e-07,
    weight_decay=0.0003,
    ema_momentum=0.99,
    name='Adam'
)

model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer = opt )
model.summary()

### --- NUMPYRIZATION DATA BEFORE TRAINING ---
training_img = np.array(training_img)
train_input_length = np.array(train_input_length)  # all must be equal length to T timesteps
train_label_length = np.array(train_label_length)  # different length (only the same in Captcha dataset)

valid_img = np.array(valid_img)
valid_input_length = np.array(valid_input_length) # all must be equal length to T timesteps
valid_label_length = np.array(valid_label_length) # different length (only the same in Captcha dataset)

### --- TRAINING ---
batch_size = 32
epochs = 500

history = model.fit(x=[training_img, train_padded_txt, train_input_length, train_label_length],
                    y=np.zeros(len(training_img)),
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_data=([valid_img, valid_padded_txt, valid_input_length, valid_label_length], [np.zeros(len(valid_img))]),
                    verbose=1,
                    callbacks=callbacks_list,
                    shuffle=True)