import os
import pathlib
import cv2
import math
import json
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.sequence import pad_sequences
# import our model, different layers and activation function 
from tensorflow.keras.layers import Dense, LSTM, Reshape, BatchNormalization, Input, Conv2D, MaxPool2D, Lambda, Bidirectional, Add, Activation
from tensorflow.keras.models import Model
from tensorflow.keras.activations import relu, sigmoid, softmax
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import CSVLogger, TensorBoard, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.layers import Reshape, Layer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from keras import initializers, regularizers, constraints
from keras.layers import Conv2D, MaxPool2D, Activation, BatchNormalization, Add, Input, Dense, Reshape, Multiply, GlobalAveragePooling2D, GlobalMaxPooling2D, AveragePooling2D, Concatenate



### --- CONSTANTS DEFINING ---
REQUIRED_WITH = 2200

_BOOL_INVERT_IMG = False
print("\n### --- Set Inverting Image To: {}\n".format(_BOOL_INVERT_IMG))

TIME_STEPS = 244
print("### --- Time Step for processing dataset: {}\n".format(TIME_STEPS))


### --- DEVICE CHECK ---
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
    # RIP Training time
    raise SystemError('GPU device not found')


### --- Only use for train_line
TRAIN_JSON = './../Data/line_data.json'
IMG_FOLDER = './../Data/line_data/' 


### --- Only use for Kalapa
# TRAIN_JSON = './../Data/kalapa_labels.json'
# IMG_FOLDER = './../Data/kalapa/' 

### --- DATA PREPARATION ---
with open(TRAIN_JSON, 'r', encoding='utf8') as f:
    train_labels = json.load(f)

### --- VIETNAMESE CHARACTER SET ---
vietnamese_char_list = [
    'a', 'à', 'á', 'ả', 'ã', 'ạ',
    'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ',
    'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ',
    'b', 'c', 'd', 'đ', 'e', 'è',
    'é', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ề',
    'ế', 'ể', 'ễ', 'ệ', 'f', 'g',
    'h', 'i', 'ì', 'í', 'ỉ', 'ĩ',
    'ị', 'j', 'k', 'l', 'm', 'n',
    'o', 'ò', 'ó', 'ỏ', 'õ', 'ọ',
    'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ',
    'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ',
    'p', 'q', 'r', 's', 't', 'u',
    'ù', 'ú', 'ủ', 'ũ', 'ụ', 'ư',
    'ừ', 'ứ', 'ử', 'ữ', 'ự', 'v',
    'w', 'x', 'y', 'ỳ', 'ý', 'ỷ',
    'ỹ', 'ỵ', 'z', 'A', 'À', 'Á',
    'Ả', 'Ã', 'Ạ', 'Ă', 'Ằ', 'Ắ',
    'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ầ', 'Ấ',
    'Ẩ', 'Ẫ', 'Ậ', 'B', 'C', 'D',
    'Đ', 'E', 'È', 'É', 'Ẻ', 'Ẽ',
    'Ẹ', 'Ê', 'Ề', 'Ế', 'Ể', 'Ễ',
    'Ệ', 'F', 'G', 'H', 'I', 'Ì',
    'Í', 'Ỉ', 'Ĩ', 'Ị', 'J', 'K',
    'L', 'M', 'N', 'O', 'Ò', 'Ó',
    'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ồ', 'Ố',
    'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ờ', 'Ớ',
    'Ở', 'Ỡ', 'Ợ', 'P', 'Q', 'R',
    'S', 'T', 'U', 'Ù', 'Ú', 'Ủ',
    'Ũ', 'Ụ', 'Ư', 'Ừ', 'Ứ', 'Ử',
    'Ữ', 'Ự', 'V', 'W', 'X', 'Y',
    'Ỳ', 'Ý', 'Ỷ', 'Ỹ', 'Ỵ', 'Z',
    '0', '1', '2', '3', '4', '5',
    '6', '7', '8', '9', '!', '@',
    '#', '$', '%', '^', '&', '*',
    '(', ')', '_', '+', '-', '=',
    '{', '}', '[', ']', '|', '\\',
    ';', ':', "'", '"', '<', '>',
    ',', '.', '/', '?', ' ', '~',
    '`']
vietnamese_char_list = sorted(vietnamese_char_list)
print("\n### --- There are {} Vietnamese tokens...\n".format(len(vietnamese_char_list))) #229


# convert the words to array of indexs based on the char_list
def encode_to_labels(txt):
    # encoding each output word into digits of indexes
    dig_lst = []
    for index, char in enumerate(txt):
        try:
            dig_lst.append(vietnamese_char_list.index(char))
        except:
            print("No found in token list :", char)
    return dig_lst


train_image_path = []

for item in pathlib.Path(IMG_FOLDER).glob('**/*'):
    if item.is_file() and item.suffix not in [".json"]:
        train_image_path.append(str(item))

dict_filepath_label={}
raw_data_path = pathlib.Path(os.path.join(IMG_FOLDER))
for item in raw_data_path.glob('**/*.*'):
    file_name=str(os.path.basename(item))
    if file_name != "labels.json":
        file_name = file_name.split(".")[0]  #only use for train_line dataset
        label = train_labels[file_name]
        dict_filepath_label[str(item)]=label

label_lens= []
for label in dict_filepath_label.values():
    label_lens.append(len(label))
MAX_LABEL_LEN = max(label_lens)

all_image_paths = list(dict_filepath_label.keys())

TEST_SIZE = 0.2
train_image_paths, val_image_paths = train_test_split(all_image_paths, test_size=TEST_SIZE, random_state=42)


# lists for training dataset
training_img = []
training_txt = []
train_input_length = []
train_label_length = []
orig_txt = []
resize_max_width=0

for train_img_path in tqdm(train_image_paths, desc="Processing Training Images"):
    # print(f_name)
    # read input image and convert into gray scale image
    img = cv2.cvtColor(cv2.imread(train_img_path), cv2.COLOR_BGR2GRAY)
    
    height, width = img.shape

    # in this dataset, we don't need to do any resize at all here.
    img = cv2.resize(img,(int(118/height*width),118))
    
    height, width = img.shape
    
    if img.shape[1] > resize_max_width:
        resize_max_width = img.shape[1]

    img = np.pad(img, ((0,0),(0, REQUIRED_WITH-width)), 'constant',constant_values=(255, 255))
    
    # Blur it
    img = cv2.GaussianBlur(img, (5,5), 0)

    # Threshold the image using adapative threshold
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,  
                                # Use THRESH_BINARY
                                 blockSize=11, C=4)
    
    # Invert the image to get black characters on white background
    if _BOOL_INVERT_IMG:
        img = 255 - img  # Invert pixel values
    
    # add channel dimension
    img = np.expand_dims(img , axis = 2)
    
    # Normalize each image
    img = img/255.

    label = dict_filepath_label[train_img_path]

    # split data into validation and training dataset as 10% and 90% respectively
    orig_txt.append(label)   
    train_label_length.append(len(label))

    # our time steps for valid input
    train_input_length.append(TIME_STEPS)
    training_img.append(img)

    # convert words to digits based on charlist
    training_txt.append(encode_to_labels(label)) 

#lists for validation dataset
valid_img = []
valid_txt = []
valid_input_length = []
valid_label_length = []
valid_orig_txt = []

for val_img_path in tqdm(val_image_paths, desc="Processing Validation Images"):
    # print(f_name)
    # read input image and convert into gray scale image
    img = cv2.cvtColor(cv2.imread(val_img_path), cv2.COLOR_BGR2GRAY)
    
    # in this dataset, we don't need to do any resize at all here.
    img = cv2.resize(img,(int(118/height*width),118))
    
    if img.shape[1] > resize_max_width:
        resize_max_width = img.shape[1]
        
    img = np.pad(img, ((0,0),(0, REQUIRED_WITH-width)), 'constant',constant_values=(255, 255))
    
    # Blur it
    img = cv2.GaussianBlur(img, (5,5), 0)

    # Threshold the image using adapative threshold
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,  
                                # Use THRESH_BINARY
                                 blockSize=11, C=4)
    
    # Invert the image to get black characters on white background
    if _BOOL_INVERT_IMG:
        img = 255 - img  # Invert pixel values
    
    # add channel dimension
    img = np.expand_dims(img , axis = 2)
    
    # Normalize each image
    img = img/255.

    label = dict_filepath_label[val_img_path]

    valid_orig_txt.append(label)   
    valid_label_length.append(len(label))

    # our time steps for valid input
    valid_input_length.append(TIME_STEPS)
    valid_img.append(img)

    # convert words to digits based on charlist
    valid_txt.append(encode_to_labels(label))


# pad each output label to maximum text length, remember we did that so that we keep training with rnn consistent?
train_padded_txt = pad_sequences(training_txt, maxlen=TIME_STEPS, padding='post', value = 0).astype('int64')
valid_padded_txt = pad_sequences(valid_txt, maxlen=TIME_STEPS, padding='post', value = 0).astype('int64')


### --- MODEL OF RESNET50 AND Bi-LSTM WITH ATTENTION ---
def channel_attention(input_tensor, reduction_ratio=16):
    channels = K.int_shape(input_tensor)[-1] 
    avg_pool = GlobalAveragePooling2D()(input_tensor)
    max_pool = GlobalMaxPooling2D()(input_tensor)
    fc1 = Dense(channels // reduction_ratio, activation='relu')(avg_pool)
    fc2 = Dense(channels, activation='relu')(fc1)
    channel_attention_weights = Activation('sigmoid')(fc2)
    channel_attention_weights = Reshape((1, 1, channels))(channel_attention_weights)
    return Multiply()([input_tensor, channel_attention_weights])

def spatial_attention(input_tensor):
    max_pool = MaxPool2D(pool_size=(2, 2), strides=(1, 1), padding='same')(input_tensor)
    avg_pool = AveragePooling2D(pool_size=(2, 2), strides=(1, 1), padding='same')(input_tensor)
    concatenated = Concatenate(axis=-1)([max_pool, avg_pool])
    spatial_attention_map = Conv2D(1, (7, 7), padding='same', activation='sigmoid')(concatenated)
    return Multiply()([input_tensor, spatial_attention_map])

def cbam_block(input_tensor):
    channel_attended = channel_attention(input_tensor)
    spatial_attended = spatial_attention(channel_attended)
    return spatial_attended


inputs = Input(shape=(118, REQUIRED_WITH, 1), name='input')

# Block 1
x = Conv2D(64, (3,3), padding='same')(inputs)
x = MaxPool2D(pool_size=3, strides=3)(x)
x = Activation('relu')(x)
x_1 = x 

# Block 2
x = Conv2D(128, (3,3), padding='same')(x)
x = MaxPool2D(pool_size=3, strides=3)(x)
x = Activation('relu')(x)
x_2 = x

# Block 3
x = Conv2D(256, (3,3), padding='same')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x_3 = x

# Block4
x = Conv2D(256, (3,3), padding='same')(x)
x = BatchNormalization()(x)
x = Add()([x,x_3])
x = Activation('relu')(x)
x_4 = x

# Block5
x = Conv2D(512, (3,3), padding='same')(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x_5 = x

# Block6
x = Conv2D(512, (3,3), padding='same')(x)
x = BatchNormalization()(x)
x = Add()([x,x_5])
x = Activation('relu')(x)

# Block7
x = Conv2D(1024, (3,3), padding='same')(x)
x = BatchNormalization()(x)
x = MaxPool2D(pool_size=(3, 1))(x)
x = Activation('relu')(x)

# pooling layer with kernel size (2,2) to make the height/2 #(1,9,512)
x = MaxPool2D(pool_size=(3, 1))(x)
 
x_cbam = cbam_block(x)

# print(x_cbam.shape)
squeezed = Lambda(lambda x: K.squeeze(x, 1))(x_cbam)
 
blstm_1 = Bidirectional(LSTM(512, return_sequences=True, dropout = 0.2))(squeezed)
blstm_2 = Bidirectional(LSTM(512, return_sequences=True, dropout = 0.2))(blstm_1)

outputs = Dense(len(vietnamese_char_list)+1, activation = 'softmax')(blstm_2)


### --- CTC LOSS DEFINE ---
# define the length of input and label for ctc
labels = Input(name='the_labels', shape=[TIME_STEPS], dtype='float32')
input_length = Input(name='input_length', shape=[1], dtype='int64')
label_length = Input(name='label_length', shape=[1], dtype='int64')

def ctc_lambda_func(args):
    y_pred, labels, input_length, label_length = args
    
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)
 

# out loss function (just take the inputs and put it in our ctc_batch_cost)
loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([outputs, labels, input_length, label_length])


### --- GET THE FULL MODEL WITH CUSTOM LOSS ---
model = Model(inputs=[inputs, labels, input_length, label_length], outputs=loss_out)


### --- LOAD PRETRAINED WEIGHTS
model.load_weights(os.path.join('model_ver20_finetune_cbam_kalapa.h5'))

for layer in model.layers:
    # Check if the layer has weights (not all layers have weights, e.g., Activation layers)
    if layer.weights:
        print(f'Layer: {layer.name}')
        weights = layer.get_weights()
        for i, w in enumerate(weights):
            print(f'  Weights {i + 1}:')
            print(w)


callbacks = [
    TensorBoard(
        log_dir='./logs/ver21_finetune_cbam_line_false',
        histogram_freq=10,
        profile_batch=0,
        write_graph=True,
        write_images=False,
        update_freq="epoch"),
    ModelCheckpoint(
        filepath="model_ver21_finetune_cbam_line_false.h5",
        monitor='val_loss',
        save_best_only=True,
        save_weights_only=True,
        verbose=1),
    EarlyStopping(
        monitor='val_loss',
        min_delta=1e-8,
        patience=50,
        restore_best_weights=True,
        verbose=1),
    ReduceLROnPlateau(
        monitor='val_loss',
        min_delta=1e-8,
        factor=0.1,
        patience=20,
        verbose=1)
]

callbacks_list = callbacks

opt = Adam(
    learning_rate=0.0001,
    beta_1=0.9,
    beta_2=0.999,
    epsilon=1e-08,
    weight_decay=0.001,
    ema_momentum=0.99,
    name='Adam'
)

model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer = opt)
model.summary()

### --- NUMPYRIZATION DATA BEFORE TRAINING ---
training_img = np.array(training_img)
train_input_length = np.array(train_input_length)
train_label_length = np.array(train_label_length)

valid_img = np.array(valid_img)
valid_input_length = np.array(valid_input_length)
valid_label_length = np.array(valid_label_length)

# print("Length of train_input: {} - train_label - {}".format(train_input_length, train_label_length))
# print("Length of valid_input: {} - valid_label - {}".format(valid_input_length, valid_label_length))

### --- TRAINING ---
batch_size = 32
epochs = 200

history = model.fit(x=[training_img, train_padded_txt, train_input_length, train_label_length],
                    y=np.zeros(len(training_img)),
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_data=([valid_img, valid_padded_txt, valid_input_length, valid_label_length], [np.zeros(len(valid_img))]),
                    verbose=1,
                    callbacks=callbacks_list,
                    shuffle=True)